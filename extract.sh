#!/bin/bash

#7zip
7z e shrek.jpg

# tar & gzip
tar -xvf flag.tar.gz

#cpio
cpio -idv <flag.cpio

#lzma
unlzma flag.lzma

#ar
ar -xv flag

#bzip2
bunzip2 flag1.txt

#xd is finnicky so you have to use a file extension lol
mv flag1.txt.out flag1.txt.xz
xz -d flag1.txt.xz

#print flag
cat flag1.txt
