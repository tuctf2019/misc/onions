# Onions

## Challenge description
Ogers are like files -- they have layers!

## Required files for hosting
`shrek.jpg`

## Hosting instructions
- The included file should be distributed with the challenge description/flag submission platform.

- compress.sh is a script for recreating the chal with a flag.txt and shrek.jpg file.
- extrach.sh is a script to extract the flag file from the included shrek.jpg file.

## Hints
- Something's hidden in the picture.
- Once you get the archived file: Keep digging away at the archive! There'll be a flag file eventually.

## Flag 
`TUCTF{F1L3S4R3L1K30N10NSTH3YH4V3L4Y3RS}`